##############################################################################################################
# G53FIV LAB ONE
##############################################################################################################

library(ggplot2) #install.packages("ggplot2")

# Display head of dataset
head(iris) # iris is a dataset built into R 

# Scatterplot
plot(x=iris$Sepal.Length, y=iris$Sepal.Width, 
     xlab="Sepal length", ylab="Sepal width",  main="Sepal length-width")

qplot(x = Sepal.Length, y = Sepal.Width, data = iris, 
      xlab="Sepal Length", ylab="Sepal Width", 
      main="Sepal Length-Width", color=Species, shape=Species)

scatter <- ggplot(data=iris, aes(x = Sepal.Length, y = Sepal.Width)) 
scatter + geom_point(aes(color=Species, shape=Species)) +
  xlab("Sepal Length") +  ylab("Sepal Width") +
  ggtitle("Sepal Length-Width")

scatter + geom_point(aes(color = Petal.Width, shape = Species), size = 2, alpha = I(1/2)) +
  geom_vline(aes(xintercept = mean(Sepal.Length)), color = "red", linetype = "dashed") +
  geom_hline(aes(yintercept = mean(Sepal.Width)), color = "red", linetype = "dashed") +
  scale_color_gradient(low = "yellow", high = "red") +
  xlab("Sepal Length") +  ylab("Sepal Width") +
  ggtitle("Sepal Length-Width")

# Histogram
hist(iris$Sepal.Width, freq=NULL, density=NULL, breaks=12,
     xlab="Sepal Width", ylab="Frequency", main="Histogram of Sepal Width")

histogram <- ggplot(data=iris, aes(x=Sepal.Width))
histogram + geom_histogram(binwidth=0.2, color="black", aes(fill=Species)) + 
  xlab("Sepal Width") +  ylab("Frequency") + ggtitle("Histogram of Sepal Width")



# ggplot2 extensions
# Four common visualizations for multi-variate data:
#   Chernoff faces 
#   Table Lens
#   Parallel Coordinates
#   Mosaic Plots

#-----Chernoff faces-----#
#-----Table Lens-----#
#-----Chernoff faces-----#
library(ggChernoff) #install.packages("ggChernoff")
ggplot(iris) +
  aes(Petal.Width, Petal.Length, fill = Species) +
  geom_chernoff()
#-----Parallel Coordinates-----#

#-----Mosaic Plots-----#
library(ggmosaic) #install.packages("ggmosaic")
ggplot(data = fly) +
  geom_mosaic(aes(x = product(RudeToRecline), fill=DoYouRecline))
