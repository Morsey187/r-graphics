library(ggplot2)
library(dplyr)
library(data.table)
library(plyr)
library(downloader)
library(plotly)
library(gganimate)
library(reshape2)
library(ggmosaic)

# Loading the data
filename <- "ive_births_country_and_age_of_mother_2008_2017.csv"
if (!file.exists(filename)) {
  print("loading file from repo")
  dfAgeCountry <- read.csv(url("https://gitlab.com/Morsey187/r-graphics/raw/master/birth-statistics/RQ3/ive_births_country_and_age_of_mother_2008_2017.csv?inline=false"))
} else {
  print("loading file locally")
  dfAgeCountry <- read.csv("ive_births_country_and_age_of_mother_2008_2017.csv")
}

##############################################################################################################
# DATA MANIPULATION 
##############################################################################################################

# Formattng ive_births_country_and_age_of_mother_2008_2017.csv
# Renaming Column
names(dfAgeCountry)[names(dfAgeCountry) == 'Country.of.birth.of.mother'] <- 'CountryOfMother'

# Deleting empty rows
dfAgeCountry=dfAgeCountry[!apply(dfAgeCountry == "", 1, all),]

# Removing leading and trailing whitespaces
dfAgeCountry$CountryOfMother=trimws(dfAgeCountry$CountryOfMother)

# Deleting Total row
#dfAgeCountry = subset(dfAgeCountry, CountryOfMother!="Total")

# Adding year value to rows, note theres 31 rows of data for each year starting from 2017
years = rep(c(2017:2008), 31)
dfAgeCountry$Year = sort(years, decreasing = TRUE)

# Using R melt function to reshape table to stack data by Age
dfAgeCountry <- melt(dfAgeCountry, variable.name="AgeOfMother", value.name="Count", id=c("CountryOfMother","Year"))

# Removing commas from Count and converting to numeric, note '-' values added by ONS represent 0 for years before 2013
dfAgeCountry$Count = gsub(',','', dfAgeCountry$Count)
dfAgeCountry$Count = gsub('-',0, dfAgeCountry$Count)
dfAgeCountry$Count = as.numeric(dfAgeCountry$Count)

# Removing X from year values (was added when loaded as header columns before reshaping)
dfAgeCountry$AgeOfMother = as.character(dfAgeCountry$AgeOfMother) # Converting to strings to edit values
dfAgeCountry$AgeOfMother <- gsub('X','', dfAgeCountry$AgeOfMother)
dfAgeCountry$AgeOfMother <- gsub('\\.',' ', dfAgeCountry$AgeOfMother)
dfAgeCountry$AgeOfMother <- gsub('Under','<', dfAgeCountry$AgeOfMother)

# Creating rows for Europe (would like to group by continents and Europe is grouped as EU and non EU by ONS)
europedf = dfAgeCountry %>% 
  group_by(Year,AgeOfMother) %>%
  filter(CountryOfMother %in% c("EU","Rest of Europe (non EU)")) %>%
  summarise(CountryOfMother="Europe",Count=sum(Count))%>%
  ungroup 
dfAgeCountry=rbind(dfAgeCountry,europedf) 
dfAgeCountry$AgeOfMother = as.factor(dfAgeCountry$AgeOfMother) # Converting back into factors
dfAgeCountry$CountryOfMother = as.factor(dfAgeCountry$CountryOfMother) # Converting Countries into factors
head(dfAgeCountry)
tail(dfAgeCountry)
str(dfAgeCountry)

write.csv(dfAgeCountry, file = "birth_mother_data_2008_2017.csv", row.names=FALSE, na="")

##############################################################################################################
# EXPLORATION
##############################################################################################################

# Viewing births by UK born and outside UK born mothers
inOutUk = filter(dfAgeCountry, 
                 CountryOfMother %in% c("Total outside United Kingdom", "United Kingdom"),
                 AgeOfMother=="All ages")
ggplot(inOutUk, aes(x=Year, y=Count, colour=CountryOfMother)) + geom_line() # Map supp to linetype

# Viewing different age groups of mothers born in the UK and birth counts
agesUk = filter(dfAgeCountry, 
                 CountryOfMother == "United Kingdom",
                 AgeOfMother!="All ages")
ggplot(agesUk, aes(x=Year, y=Count, color=AgeOfMother)) + geom_line()

# Changes in birth mothers age 
agesUk = filter(dfAgeCountry, 
                CountryOfMother == "Total",
                AgeOfMother!="All ages")
ggplot(agesUk, aes(x=Year, y=Count, color=AgeOfMother)) + geom_line()


# Viewing Birth Count in relation to mothers age and country of birth
ageCountry2017 = filter(dfAgeCountry, Year==2017,
              CountryOfMother %in% c("Europe","Africa","The Americas and the Caribbean","Middle East and Asia","Antarctica and Oceania"),
              AgeOfMother!="All ages")
# Exploring the use of stacked bar graphs
ggplot(ageCountry2017, aes(x=CountryOfMother, y=Count, fill=AgeOfMother)) +
  geom_bar(stat="identity") + 
  theme(axis.text.x = element_text(angle=25, vjust=0.7)) 
ggplot(ageCountry2017, aes(x=AgeOfMother, y=Count, fill=CountryOfMother)) +
  geom_bar(stat="identity") + 
  scale_fill_brewer(palette="Set1") +
  coord_flip() +
  theme(legend.position=c(1,1), legend.justification=c(1,1))
# Difficult to visualise counts of different categories

# Scatter Plot Animation, 
temp = dfAgeCountry %>%
  group_by(AgeOfMother) %>%
  filter(CountryOfMother %in% c("Europe","Africa","The Americas and the Caribbean","Middle East and Asia","Antarctica and Oceania"),
         AgeOfMother!="All ages")
p <-ggplot(temp, aes(Count, AgeOfMother, colour = CountryOfMother)) +
  geom_point(alpha = 0.7) +
  theme(legend.position=c(1,1), legend.justification=c(1,1)) +
  labs(title = 'Year: {frame_time}', 
       x = 'Birth Count', y = 'Mother\'s Age Group',
       color="Mother\'s Country of Birth") 
p
# Uncomment to animate (will take around 30 seconds to render)
#p + transition_time(Year) +
#  ease_aes('linear')

# Same animation but using facets 
p <- ggplot(temp, aes(CountryOfMother, Count, colour = CountryOfMother)) +
  geom_point(alpha = 0.7, show.legend = FALSE) +
  facet_wrap(~AgeOfMother) +
  theme(axis.text.x=element_text(angle = -30, hjust = 0)) +
  labs(title = 'Year: {frame_time}', x = 'Continent', y = 'Birth Count')
p
#p + transition_time(Year) +
#  ease_aes('linear')

# All graphs have the same problem of viewing different counts accurately accross categories
# Normal bar graph seems more effective 
ggplot(ageCountry2017, aes(x=CountryOfMother, y=Count, fill=AgeOfMother)) +
  geom_bar(stat="identity", position="dodge") +
  theme(axis.text.x=element_text(angle = -30, hjust = 0)) 

# Overlapping bars instead of stacking, and changing the order of countries by value 
p <- ggplot(ageCountry2017) + coord_flip() +
  theme(legend.position=c(1,0.1), legend.justification=c(1,0.2)) +
  labs(title="Birth Count by Mother's Characteristics: UK 2017", 
       x = 'Country of Birth of Mother', y = 'Birth Count') 
p1 <- p + geom_bar(aes( reorder(CountryOfMother, Count), Count), stat = "identity")
p2 <- p1 + geom_bar(aes(CountryOfMother, Count, fill = AgeOfMother),stat = "identity", position = "dodge")
p2

# Although viewing the different categories and understading the data is much easier to 
# diegest in this format the smaller Count values are still far too difficult to interpret
# Decieded to turn values into percentages and create a Mosaic chart, since counting the 
# exact values (specifcally the smaller ones) don't seem as important as viewing the distribution of them

# Mosaic Chart
# Converting Count to Percent
ageCountryPercent2017 = filter(dfAgeCountry, Year==2017,
                        CountryOfMother %in% c("United Kingdom","Europe","Africa","The Americas and the Caribbean","Middle East and Asia","Antarctica and Oceania"),
                        AgeOfMother!="All ages") %>%
  mutate(Percent = Count / sum(Count) * 100) %>%
  arrange(AgeOfMother,CountryOfMother)
# Renaming Continents 
ageCountryPercent2017$CountryOfMother <- gsub('Middle East and Asia','Asia', ageCountryPercent2017$CountryOfMother)
ageCountryPercent2017$CountryOfMother <- gsub('Antarctica and Oceania','Antarctica & Oceania', ageCountryPercent2017$CountryOfMother)
ageCountryPercent2017$CountryOfMother <- gsub('The Americas and the Caribbean','Americas', ageCountryPercent2017$CountryOfMother)
# Ordering factors for mosaic manually (by level) since geom_mosaic does not support ordering
ageCountryPercent2017$CountryOfMother <- factor(ageCountryPercent2017$CountryOfMother, levels = c("United Kingdom","Europe","Asia","Africa","Americas","Antarctica & Oceania"))
# Converting to characters and back just to remove left over level "All ages" which 
# geom_mosaic reads regardless of filtering
ageCountryPercent2017$AgeOfMother = as.character(ageCountryPercent2017$AgeOfMother) 
ageCountryPercent2017$AgeOfMother = as.factor(ageCountryPercent2017$AgeOfMother) 

ggplot(data = ageCountryPercent2017) +
  geom_mosaic(aes(weight=Percent,
                  x = product(CountryOfMother),
                  fill=AgeOfMother))

##############################################################################################################
# RQ3 MAIN VISUALIZATION
##############################################################################################################

message("RQ3: How do births change in relation to birth mothers age and ethnicity?")

p <- ggplot(data = ageCountryPercent2017) +
  geom_mosaic(aes(weight=Percent,
                  x = product(CountryOfMother),
                  fill=AgeOfMother)) +
  scale_fill_brewer(palette = "Dark2",guide = guide_legend(reverse=TRUE), name="Age of Mother") + 
  labs(title="Percentage of Births by Mother's Characteristics: UK 2017", 
       x="Location of Mother\'s Birth") +
  theme(plot.title = element_text(family = "Helvetica",face = "bold", size = (15)),
        plot.subtitle = element_text(size=12),
        plot.background = element_rect(fill = "transparent", color = NA),
        panel.background = element_rect(fill = "transparent"),panel.grid = element_blank(),
        axis.text.y = element_blank(),axis.title.y = element_blank(),axis.ticks =element_blank(),
        axis.text.x = element_text(angle = -45, hjust = 0))
p
png(file="birth_mother_mosaic_chart.png", width=800, height=600)
p
dev.off()



##############################################################################################################
# EXPANDING ON RQ3
##############################################################################################################

message("RQ3.1: How do teen births from foreign born mothers change over the years?")

# Teen births (under 20 years), by Mother's Continent of birth
teenPlusContinents = dfAgeCountry
teenPlusContinents = filter(teenPlusContinents,CountryOfMother %in% c("Europe","Africa","The Americas and the Caribbean","Middle East and Asia","Antarctica and Oceania"))
teenPlusContinents = teenPlusContinents %>%
  mutate(Percent = Count / sum(Count) * 100) 
  
teenPlusContinents = filter(teenPlusContinents,AgeOfMother=="< 20")
teenPlusContinents$CountryOfMother = as.character(teenPlusContinents$CountryOfMother) 
teenPlusContinents$CountryOfMother = as.factor(teenPlusContinents$CountryOfMother) 
teenPlusContinents$CountryOfMother <- factor(teenPlusContinents$CountryOfMother, levels = c("Europe","Middle East and Asia","Africa","The Americas and the Caribbean","Antarctica and Oceania"))
p <- ggplot(data = teenPlusContinents) +
  geom_mosaic(aes(weight=Percent,
                  x = product(Year),
                  fill=CountryOfMother)) +
  scale_fill_brewer(palette = "Dark2",guide = guide_legend(reverse=TRUE), name="Continent of\nMother's Birth") + 
  labs(title="Teen Births by Foreign Mothers: UK 2008 - 2017",
       subtitle="Teen Births = Mother's under 20 years") +
  theme(plot.title = element_text(family = "Helvetica",face = "bold", size = (15)),
        plot.subtitle = element_text(size=12),
        plot.background = element_rect(fill = "transparent", color = NA),
        panel.background = element_rect(fill = "transparent"),panel.grid = element_blank(),
        axis.text.y = element_blank(),axis.title = element_blank(),axis.ticks =element_blank())
p
png(file="birth_teen_immigration_mosaic_chart.png", width=800, height=600)
p
dev.off()


message("RQ3.2: How does the age group of mothers change over time?")

# Proportional Stacked Area Graph
# Filtering out All ages factor and looking only at birth totals
temp = filter(dfAgeCountry, CountryOfMother == "Total",AgeOfMother!="All ages")
# Converting Count to Percent
temp = temp %>%
  group_by(Year) %>%
  mutate(Percent = Count / sum(Count) * 100) %>%
  ungroup()
# Plotting
p = ggplot(temp, aes(x=Year, y=Percent, fill=AgeOfMother)) + 
  geom_area(colour=NA, alpha=.3) +
  scale_fill_brewer(palette = "Blues", name="Age of\nMother") + 
  geom_line(position="stack", size=.3)  +
  scale_x_continuous(breaks = temp$Year) +
  scale_y_continuous(labels = function(x) paste0(x, "%")) +
  labs(title="Births by Age of Mothers", subtitle="United Kingdom 2008 - 2017") +
  theme(plot.title = element_text(family = "Helvetica",face = "bold", size = (15),hjust = 0.5),
        plot.subtitle = element_text(size=12,hjust = 0.5),
        plot.background = element_rect(fill = "transparent", color = NA),panel.background = element_rect(fill = "transparent"),
        panel.grid.major = element_line(size = 0.1, linetype = 'solid',colour = "lightblue"), panel.grid.minor = element_line(size = 0.1, linetype = 'solid',colour = "lightblue"),
        axis.title = element_blank(),axis.text.x = element_text(vjust = 2),axis.text.y = element_text(hjust = 2),axis.ticks =element_blank()
   )
p
png(file="birth_age_mother_proportional_stacked_area_graph.png", width=800, height=600)
p
dev.off()

