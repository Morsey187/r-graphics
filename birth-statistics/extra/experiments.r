library(ggplot2)
library(dplyr)
library(rgdal)
library(ggmap)
library(broom)
############### Uncomment to create pngs for bar chart gif
#dir1 <- file.path(".", "birth_count_pngs")
#if(!dir.exists(dir1)) {
#  dir.create(dir1)
#}
#maxNum = summarize(df, max = max(count))
#NumYears = 80
#PlotBarGraph <- function(j) {
#  record = filter(df, year == year[j])
#  ggplot(data=record, aes(x=month, y=count, fill=month)) +
#    geom_bar(stat="identity") + 
#    coord_cartesian(xlim=c(0.5,12.5), ylim=c(0, maxNum$max)) +
#    my_theme +
#    ggtitle("Monthly Birth Count ", record$year) + 
#    labs(fill = "Months")
#}
#for(i in 1:NumYears) {
#  print(paste("Plot ", i, " of ", NumYears))
#  j=81-i
#  p1 <- PlotBarGraph(j)
#  ggsave(filename = file.path(dir1, paste0("img", sprintf("%04d", i), ".png")),plot = p1)
#}  
#list.files(path = "./birth_count_pngs/")
# created gif using http://gifmaker.org
###############
# Exploring data some more
############### Used function to create pngs for bar chart gif
# GIF Bar Graph
#maxNum = summarize(df,max = max(avgDayCount))
#dir1 <- file.path(".", "birth_count_adj_pngs")
#if(!dir.exists(dir1)) {
#  dir.create(dir1)
#}
#PlotBarGraph2 <- function(j) {
#  record = filter(df, year == year[j])
#  ggplot(data=record, aes(x=month, y=avgDayCount, fill=month)) +
#    geom_bar(stat="identity") + 
#    coord_cartesian(xlim=c(0.5,12.5), ylim=c(0, maxNum$max)) +
#    my_theme +
#    ggtitle("Monthly Birth Count ", record$year) + 
#    labs(fill = "Months")
#}
#for(i in 1:NumYears) {
#  print(paste("Plot ", i, " of ", NumYears))
#  j=81-i
#  p1 <- PlotBarGraph2(j)
#  ggsave(
#    filename = file.path(dir1, paste0("img", sprintf("%04d", i), ".png")), plot = p1)
#}  
# list.files(path = "./birth_count_adj_pngs/")
# created gif using http://gifmaker.org
###############


# readOGR: Read OGR Vector Maps Into Spatial Objects
shapefile <- readOGR("./ShapeFile/", "Counties_and_Unitary_Authorities_December_2015_Generalised_Clipped_Boundaries_in_England_and_Wales")

shapefile_df <- fortify(shapefile)

shapefile_df = shapefile_df[,!(names(shapefile_df) %in% c("order","piece","hole"))] # Remove unwanted columns, move to new DF
head(shapefile_df)
names(shapefile_df) <- c("Long","Lat","Mapid","Group") # Assigning column names
shapefile_df$Mapid <- as.integer(shapefile_df$Mapid)

mapnames <- read.csv("Counties_and_Unitary_Authorities_December_2015_Generalised_Clipped_Boundaries_in_England_and_Wales.csv")
mapnamesdf = mapnames[,(names(mapnames) %in% c("ctyua15cd","objectid"))] # Remove unwanted columns, move to new DF
names(mapnamesdf) <- c("GSSCode","Mapid") # Assigning column names



mapdf = merge(shapefile_df,mapnamesdf, by=c("Mapid"),all=TRUE)

head(mapdf)
tail(mapdf)
str(mapdf)
barDF = mapdf %>% 
  select(Long, Mapid, Group) %>%
  arrange(desc(Mapid)) 
head(barDF)
tail(barDF)

map <- ggplot() +
  geom_polygon(data = mapdf, 
               aes(x = Long, y = Lat, group = Group,fill = 'white'),
               color = 'gray', size = .2)

print(map) 


if in long lat 
get group
  mutate CBR 




map <- ggplot() +
  geom_polygon(data = shapefile_df, 
            aes(x = Long, y = Lat, group = Group),
            color = 'gray', fill = 'white', size = .2)

print(map) 





#Create some data to use in the heatmap - here we are creating a random "value" for each county (by id)
mydata <- data.frame(id=unique(mapdata$id), value=sample(c(0:100), length(unique(mapdata$id)), replace = TRUE))
head(mydata)
#Join mydata with mapdata
df <- join(mapdata, mydata, by="id")

#Create the heatmap using the ggplot2 package
gg <- ggplot() + geom_polygon(data = df, aes(x = long, y = lat, group = group, fill = value), color = "#FFFFFF", size = 0.25)
gg <- gg + scale_fill_gradient2(low = "blue", mid = "red", high = "yellow", na.value = "white")
gg <- gg + coord_fixed(1)
gg <- gg + theme_minimal()
gg <- gg + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), legend.position = 'none')
gg <- gg + theme(axis.title.x=element_blank(), axis.text.x = element_blank(), axis.ticks.x = element_blank())
gg <- gg + theme(axis.title.y=element_blank(), axis.text.y = element_blank(), axis.ticks.y = element_blank())
print(gg)

gg

gg









#mapnames <- mapdata %>%
#  select(long,lat,id) %>%
#  dplyr::distinct(id, .keep_all = TRUE) # Remove duplicates so we're left with just map names and one set of coords for it to pair later








# Clean up 
# Give plots proper title names and discription  such as divided by month



liveBirthMonth <- read.csv("live-births-by-month-1938-2017.csv")
# Convert factors to numericals
cols <- setdiff(names(liveBirthMonth),"Year") # list of column names excluding Year
for (i in 1:length(cols))
  liveBirthMonth[cols[i]] <- as.numeric(gsub(',', '', liveBirthMonth[[cols[i]]]))
str(liveBirthMonth)
# Better formated DF
df = data.frame(year = numeric(0), month = character,count = numeric(0));
cols <- setdiff(names(liveBirthMonth),c("Year","Total")) # list of column names excluding Year
for (i in 1:length(cols)){
  print(i)
  record = data.frame(year = liveBirthMonth$Year,
                      month=cols[i],
                      count=liveBirthMonth[[cols[i]]]
  )
  df = rbind(df, record)
}
# Add Totals back for each year
df = df %>%
  dplyr::group_by(year) %>%
  dplyr::mutate(total = sum(count)) %>%
  ungroup()









maxNum = summarize(df, max = max(count))

MaxCount = maxNum$max
head(MaxCount)
NumYears <- 80
graphics.off()

dir1 <- file.path(".", "birth_count_pngs")
if(!dir.exists(dir1)) {
  dir.create(dir1)
}


my_theme <- theme(plot.title = element_text(family = "Helvetica", face = "bold", size = (15)), 
                  plot.subtitle =element_text(size=18, hjust=0.5, face="italic", color="black"),    
                  legend.title = element_text(colour = "steelblue4",  face = "bold.italic", family = "Helvetica"), 
                  legend.text = element_text(face = "italic", colour="steelblue4",family = "Helvetica"), 
                  axis.title = element_text(family = "Helvetica", size = (10), colour = "steelblue4"),
                  axis.text = element_text(colour = "steelblue4", size = (10)),
                  axis.text.x = element_text(angle = 35, hjust = 1.1)
)

transparent_theme <- theme(plot.title = element_text(family = "Helvetica", colour="#bfbfbf",face = "bold", size = (15)), 
                           plot.subtitle =element_text(size=18, hjust=0.5, face="italic", colour="#bfbfbf"),    
                           legend.title = element_text(colour = "#bfbfbf",  face = "bold.italic", family = "Helvetica"), 
                           legend.text = element_text(face = "italic", colour="#bfbfbf",family = "Helvetica"), 
                           axis.title = element_text(family = "Helvetica", size = (10), colour = "#bfbfbf"),
                           axis.text = element_text(colour = "#bfbfbf", size = (10)),
                          axis.text.x = element_text(angle = 35, hjust = 1.1),
                          panel.background = element_rect(fill = "transparent"),
                         plot.background = element_rect(fill = "transparent", color = NA)

                          , panel.grid.major = element_blank() # get rid of major grid
                          #, panel.grid.minor = element_blank() # get rid of minor grid
                , legend.background = element_rect(fill = "transparent") # get rid of legend bg
          , legend.box.background = element_rect(fill = "transparent") # get rid of legend panel bg
          )
                                                                                    

PlotGraph <- function(j) {
  
  record = filter(df, year == year[j])
  ggplot(data=record, aes(x=month, y=count, fill=month)) +
    geom_bar(stat="identity") + 
    coord_cartesian(xlim=c(0.5,12.5), ylim=c(0, MaxCount)) +
    transparent_theme +
    ggtitle("Monthly Birth Count ", record$year) + 
    labs(fill = "Months")
  
}

for(i in 1:NumYears) {
  
  cat(paste("Plot ", i, " of ", NumYears, ": ", round(i / NumYears * 100, 1), "% complete.\n"), sep = "")
  j=81-i
  p1 <- PlotGraph(j)
  
  ggsave(
    filename = file.path(dir1, paste0("img", sprintf("%04d", i), ".png")),
    bg = "transparent",
    plot = p1)
  #ggsave(filename = file.path(dir1, paste0("img", sprintf("%04d", i), ".png")),plot = p1)
}  
list.files(path = "./birth_count_pngs/")
# http://gifmaker.org








daysInMonth = c(31,28,31,30,31,30,31,31,30,31,30,31)
daysInMonthLeapYear = c(31,29,31,30,31,30,31,31,30,31,30,31) # Feb 29 days in leap years,

# Using months factor level to compare with daysInMonth list
newTable = mutate(df, 
                  adjustedCount = 
                    ifelse(year %% 4 != 0, 
                           # common year
                           count / daysInMonth[match(month,levels(month))],
                           ifelse(year %% 100 != 0, 
                                  # leap year
                                  count / daysInMonthLeapYear[match(month,levels(month))],
                                  ifelse(year %% 400 != 0, 
                                         # common year
                                         count / daysInMonth[match(month,levels(month))], 
                                         # leap year
                                         count / daysInMonthLeapYear[match(month,levels(month))])) ) 
)
maxNum = newTable %>%
  dplyr::summarize(max = max(adjustedCount))

MaxCount = maxNum$max
head(MaxCount)
NumYears <- 80
graphics.off()

dir1 <- file.path(".", "birth_count_adj_pngs")
if(!dir.exists(dir1)) {
  dir.create(dir1)
}
PlotGraph2 <- function(j) {
  
  record = filter(newTable, year == year[j])
  ggplot(data=record, aes(x=month, y=adjustedCount, fill=month)) +
    geom_bar(stat="identity") + 
    coord_cartesian(xlim=c(0.5,12.5), ylim=c(1000, MaxCount)) +
    transparent_theme +
    ggtitle("Monthly Birth Count ", record$year) + 
    labs(fill = "Months")
  
}

for(i in 1:NumYears) {
  
  cat(paste("Plot ", i, " of ", NumYears, ": ", round(i / NumYears * 100, 1), "% complete.\n"), sep = "")
  j=81-i
  p1 <- PlotGraph2(j)
  
  ggsave(
    filename = file.path(dir1, paste0("img", sprintf("%04d", i), ".png")),
    bg = "transparent",
    plot = p1)
}  
list.files(path = "./birth_count_adj_pngs/")
# http://gifmaker.org







library(ggTimeSeries)

marSep = filter(df, year  >= 2007, month %in% c("September", "March", "January"))
yeardf = filter(df, year  >= 2007)
p1 = ggplot(marSep, aes(x = year, y = avgDayPercent, group = month, fill = month)) +
  stat_steamgraph()
p1 + 
  xlab('Years') + 
  ylab('Count') 
#+ coord_fixed( 0.2 * diff(range(dfData$Time)) / diff(range(dfData$Signal)))



plotdata <- df %>%
  dplyr::group_by(year) %>%
  dplyr::mutate(ordering = rank(avgDayCount)) %>%
  ungroup() 
head(plotdata)

dfTemp = plotdata %>% 
  arrange(desc(year), ordering) 
head(dfTemp)

dfTemp = dfTemp %>%
  dplyr::group_by(month) %>%
  dplyr::filter(year>=2000,year<=2017)


p <-ggplot(dfTemp, aes(x=ordering, y=avgDayCount), alpha = 0.9, fill = "blues") +
  geom_bar(stat="identity",position = "dodge") +
  # text on top of bars
  # text in x-axis (requires clip = "off" in coord_cartesian)
  coord_flip(clip = "off", expand = FALSE) +
  my_theme +
  geom_text(aes(y = 0, label = month)) +
  
  scale_fill_viridis_d(name="")+
  theme(plot.title = element_text(hjust = 1, size = 22),
        axis.ticks.y = element_blank(),
        axis.text.y  = element_blank()) + 
  
  ggtitle("Monthly Birth Count","{frame_time}") +
  labs(fill = "Months",x = '', y = 'Average Daily Birth Count (births / days in month)') +
  transition_time(year)+
  ease_aes('cubic-in-out')

animate(p, nframes = 300, fps = 20, width = 400, height = 300)

