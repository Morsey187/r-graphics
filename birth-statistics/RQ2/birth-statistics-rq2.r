library(ggplot2)
library(dplyr)
library(data.table)
library(plyr)
library(downloader)
library(reshape2)
library(rgdal)
library(ggmap)
library(broom)
library(plotly)

# Loading the data
filename <- "live_birth_area_residence_2017.csv"
if (!file.exists(filename)) {
  print("loading file from repo")
  liveBirthLocation <- read.csv(url("https://gitlab.com/Morsey187/r-graphics/raw/master/birth-statistics/RQ2/live_birth_area_residence_2017.csv?inline=false"))
} else {
  print("loading file locally")
  liveBirthLocation <- read.csv("live_birth_area_residence_2017.csv", header=FALSE)
}

filename2 <- "Counties_and_Unitary_Authorities_December_2015_Generalised_Clipped_Boundaries_in_England_and_Wales.csv"
if (!file.exists(filename2)) {
  print("loading file from repo")
  mapnames <- read.csv(url("https://gitlab.com/Morsey187/r-graphics/raw/master/birth-statistics/RQ2/Counties_and_Unitary_Authorities_December_2015_Generalised_Clipped_Boundaries_in_England_and_Wales.csv?inline=false"))
} else {
  print("loading file locally")
  mapnames <- read.csv("Counties_and_Unitary_Authorities_December_2015_Generalised_Clipped_Boundaries_in_England_and_Wales.csv", header=TRUE)
}
#----Need to be in the right folder directory to load shape files ----#
print("Need to be in the right folder directory to load shape files")
shapefile <- readOGR("./ShapeFile/", "Counties_and_Unitary_Authorities_December_2015_Generalised_Clipped_Boundaries_in_England_and_Wales")


##############################################################################################################
# DATA MANIPULATION 
##############################################################################################################

# shapefile data
shapefile_df <- fortify(shapefile) # Converting shapefile into a data frame 
shapefile_df = shapefile_df[,!(names(shapefile_df) %in% c("piece","hole"))] # Remove unwanted columns, move to new DF
names(shapefile_df) <- c("Long","Lat","Order","Mapid","Group") # Assigning column names
shapefile_df$Mapid <- as.integer(shapefile_df$Mapid)

# mapnames data, used to match GSS Codes to shapefile data frame
mapnamesdf = mapnames[,(names(mapnames) %in% c("ctyua15cd","objectid"))] # Remove unwanted columns, move to new DF
names(mapnamesdf) <- c("GSSCode","Mapid") # Assigning column names
vecIndex = (1:nrow(mapnamesdf))-1 
mapnamesdf$Mapid = vecIndex # Applying values based on order and row indices 

mapdf = join(shapefile_df,mapnamesdf,by="Mapid") # Joining mapnamesdf to shapefile to get GSS Codes

# Birth Count Data
names(liveBirthLocation) <- c("GSSCode","Residence","Total","Male","Female","Column6","Column7","Column8","Column9","Column10","CBR") # Assigning column names
liveBirthLocationdf = liveBirthLocation[,!(names(liveBirthLocation) %in% c("Column6","Column7","Column8","Column9","Column10"))] # Remove unwanted columns, move to new DF
liveBirthLocationdf = filter(liveBirthLocationdf, grepl("W06|E10|E06|E08|E09", GSSCode)) # Filter areas to England & Wales counties, also removes NAs produced by empty rows from csv
cols <- setdiff(names(liveBirthLocationdf),c("GSSCode","Residence","CBR")) # list excluding some column names 
for (i in 1:length(cols))
  liveBirthLocationdf[cols[i]] <- as.numeric(gsub(',', '', liveBirthLocationdf[[cols[i]]])) # Removing commas and converting to numeric

liveBirthLocationdf$CBR <- as.numeric(levels(liveBirthLocationdf$CBR))[liveBirthLocationdf$CBR] # converting birth rate to numeric

liveBirthLocationdf <- liveBirthLocationdf[c("GSSCode", "Residence","Male","Female","Total","CBR")] # Reordering for readability

mapdf$GSSCode <- as.character(levels(mapdf$GSSCode))[mapdf$GSSCode] 
liveBirthLocationdf$GSSCode <- as.character(levels(liveBirthLocationdf$GSSCode))[liveBirthLocationdf$GSSCode] 

# Joining birth count data to shapefile data (coordinates and mapping data)
finaldf <- join(mapdf, liveBirthLocationdf, by="GSSCode")
head(finaldf)

write.csv(finaldf, file = "birth_by_counties_2017.csv", row.names=FALSE, na="")

##############################################################################################################
# EXPLORATION
##############################################################################################################
# Custom theme specifically made for Maps by removing grids and positioning legend
transparent_theme <- theme(plot.title = element_text(family = "Helvetica",face = "bold", size = (15)),plot.subtitle =element_text(size=12),plot.background = element_rect(fill = "transparent", color = NA),
                           panel.background = element_rect(fill = "transparent"),panel.grid = element_blank(),
                           axis.title = element_blank(),axis.text = element_blank(),axis.ticks =element_blank(),
                           legend.box.background = element_rect(fill = "transparent"),legend.position=c(.95,.95), legend.justification=c(1,1))



# Choropleth, Crude Birth Rate (Plotly's hover function was used to test values were matched to the correct counties)
p <- ggplot() + 
  geom_polygon(data = finaldf, aes(x=Long, y=Lat, group=Group, fill=CBR),color = 'gray', size = .2) +
  scale_fill_gradient2(low="white", mid="yellow", high="red", midpoint=10, na.value = "gray") +
  transparent_theme 
p <- ggplotly(p
              #,width = 800
) %>%
  config(displayModeBar=F) %>% 
  layout(title = "Birth Rates: England & Wales 2017",titlefont=list(family = "sans serif",size = 18))
p

# Bar Chart Highest Birth Count
barDF = liveBirthLocationdf %>% 
  select(Residence, Total, CBR) %>%
  arrange(desc(Total)) %>%
  slice(0:10)
ggplot(barDF, aes(x=reorder(Residence,-Total), y=Total)) + 
  geom_bar(stat="identity")

# Bar Chart Highest Birth Rates
barDF = liveBirthLocationdf %>% 
  select(Residence, Total, CBR) %>%
  arrange(desc(CBR)) %>%
  slice(0:10)
ggplot(barDF, aes(x=reorder(Residence,-CBR), y=CBR)) + 
  geom_bar(stat="identity")


##############################################################################################################
# RQ1 MAIN VISUALIZATION
##############################################################################################################

message("RQ2: How do birth rates compare across counties?")

p <- ggplot() + 
  geom_polygon(data = finaldf, aes(x=Long, y=Lat, group=Group, fill=CBR),
  color = 'gray', size = .2) +
  scale_fill_gradient2(low="white", mid="yellow", high="red", midpoint=9, na.value = "gray") +
  ggtitle("Birth Rates: England & Wales 2017","Crude Birth Rate (CBR): all births per 1,000 population of all ages") +
  transparent_theme 
p
png(file="Birth_Rate_Choropleth_2017.png") 
p
dev.off()
getOption("device")

##############################################################################################################
# EXPANDING ON RQ2
##############################################################################################################

message("RQ2.1:How does the predominant birth gender vary across counties?")

# Creating a column which displays which gender has the most births
genderdf = finaldf %>%
  group_by(GSSCode) %>%
  mutate(PopGender = ifelse(Male > Female, "Male","Female")) %>% 
  ungroup()
# Choropleth showing blue or pink dpendent on the highest count of Male vs Female
p <- ggplot() + 
  geom_polygon(data = genderdf, aes(x=Long, y=Lat, group=Group, fill=PopGender),color = 'gray', size = .2) +
  scale_fill_manual(values = c("#A418CE","#5D83EC"),na.value = "gray") +
  transparent_theme +
  ggtitle("Most Popular Birth Gender: England & Wales 2017","Female birth count vs Male birth count") 

p
png(file="Birth_Gender_Choropleth_2017.png") 
p
dev.off()
getOption("device")


message("RQ2.2:How do birth rates compare across London Boroughs?")

# Assigning areas not in london an NA value toshow up grey on the map
londondf = finaldf 
londondf$CBR <- ifelse(grepl("E09",londondf$GSSCode),  londondf$CBR, NA) 
# Birth Rate, London Bourghs, Choropleth
p <- ggplot() + 
  geom_polygon(data = londondf, aes(x=Long, y=Lat, group=Group, fill=CBR),
               color = 'gray', size = .2) +
  scale_fill_gradient2(low="white", mid="yellow", high="red", midpoint=10, na.value = "gray") +
  coord_cartesian(xlim = c(-0.6, 0.5),ylim = c(51.15, 51.75)) + # Zooming in by specifiying area of graph coordinates
  transparent_theme +
  ggtitle("Birth Rates: London Boroughs 2017","Crude Birth Rate (CBR): all births per 1,000 population of all ages") 
p
png(file="Birth_Rate_London_Choropleth_2017.png") 
p
dev.off()
getOption("device")
