library(ggplot2)
library(dplyr)
library(data.table)
library(plyr)
library(gridExtra)
library(downloader)
library(scales)
library(plotly)
library(reshape2)

library(gganimate)
library(animation)
library(gifski)
library(png)

# Loading the data
filename <- "live-births-by-month-1938-2017.csv"
if (!file.exists(filename)) {
  print("loading file from repo")
  liveBirthMonth <- read.csv(url("https://gitlab.com/Morsey187/r-graphics/raw/master/birth-statistics/RQ1/live-births-by-month-1938-2017.csv?inline=false"))
} else {
  print("loading file locally")
  liveBirthMonth <- read.csv("live-births-by-month-1938-2017.csv")
}

##############################################################################################################
# DATA MANIPULATION 
##############################################################################################################

# Convert factors to numericals
cols <- setdiff(names(liveBirthMonth),"Year") # list of column names excluding Year
for (i in 1:length(cols))
  liveBirthMonth[cols[i]] <- as.numeric(gsub(',', '', liveBirthMonth[[cols[i]]])) # Removing unwanted commas and converting to numeric
str(liveBirthMonth)
head(liveBirthMonth)
# Transposing and creating a new more appropriately formated data frame for ggplot2
df = data.frame(year = numeric(0), month = character,count = numeric(0));
cols <- setdiff(names(liveBirthMonth),c("Year","Total")) # list of column names excluding Year and Total
for (i in 1:length(cols)){
  print(i)
  record = data.frame(year = liveBirthMonth$Year,
                 month=cols[i],
                 count=liveBirthMonth[[cols[i]]])
  df = rbind(df, record)
}
head(df)
tail(df)
str(df)

# Add Totals back for each year
df = df %>%
  dplyr::group_by(year) %>%
  dplyr::mutate(total = sum(count)) %>%
  ungroup()
str(df)

# Saw a pattern of short months for every year (specifically feb)
# Dividing months by days in month to make figures more accurate 
daysInMonth = c(31,28,31,30,31,30,31,31,30,31,30,31)
daysInMonthLeapYear = c(31,29,31,30,31,30,31,31,30,31,30,31) # Feb 29 days in leap years,
# Using year and months factor level to compare with daysInMonth list
df = mutate(df, 
            avgDayCount = 
              ifelse(year %% 4 != 0, 
                     # common year
                     count / daysInMonth[match(month,levels(month))],
                     ifelse(year %% 100 != 0, 
                            # leap year
                            count / daysInMonthLeapYear[match(month,levels(month))],
                            ifelse(year %% 400 != 0, 
                                   # common year
                                   count / daysInMonth[match(month,levels(month))], 
                                   # leap year
                                   count / daysInMonthLeapYear[match(month,levels(month))])) ) 
)
# Categorising by generation
df = df %>%
  group_by(year) %>%
  mutate(generation = 
           ifelse(year >= 1944 && year <= 1964, 
                  "Baby Boomers",
                  ifelse(year >= 1965 && year <= 1979, 
                         "Gen X",
                         ifelse(year >= 1980 && year <= 1994, 
                                "Millennials",
                                ifelse(year >= 1995 && year <= 2015, 
                                       "Gen Z",
                                       NA
                                ) ) ) )
  ) %>% 
  ungroup()
head(df)

df = df %>%
  dplyr::group_by(year) %>%
  dplyr::mutate(avgDayTotal = sum(avgDayCount)) %>%
  ungroup()
head(df)
df = df %>%
  dplyr::group_by(year) %>%
  dplyr::mutate(avgDayPercent = round(avgDayCount/avgDayTotal* 100,digits=2)) %>%
  ungroup()
head(df)
str(df)

write.csv(df, file = "birth_count_data_1938_2017.csv", row.names=FALSE, na="")

##############################################################################################################
# EXPLORATION
##############################################################################################################

# Exploring data
ggplot(df, aes(x=year, y=total)) + 
  geom_bar(stat="identity")

ggplot(df, aes(x=year, y=total)) + 
  geom_line() 

birthCountByMonth = df %>%
  dplyr::group_by(month) %>%
  dplyr::filter(year>=2000,year<=2010)
ggplot(birthCountByMonth, aes(x=year, y=count, color=month)) +
  geom_line()

barDF = filter(df, year == 2017)
ggplot(barDF, aes(x=month, y=count)) + 
  geom_bar(stat="identity")

# GIF Bar Graph
my_theme <- theme(plot.title = element_text(family = "Helvetica", face = "bold", size = (15)), 
                  plot.subtitle =element_text(size=18, hjust=0.5, face="italic", color="black"),    
                  legend.title = element_text(colour = "steelblue4",  face = "bold.italic", family = "Helvetica"), 
                  legend.text = element_text(face = "italic", colour="steelblue4",family = "Helvetica"), 
                  axis.title = element_text(family = "Helvetica", size = (10), colour = "steelblue4"),
                  axis.text = element_text(colour = "steelblue4", size = (10)),
                  axis.text.x = element_text(angle = 35, hjust = 1.1)
)


# Heat Map
p <- ggplot(df, aes(x=year, y=month, fill=avgDayCount))
p + geom_tile()
# Good for showing birth count over time, not so much for comparing months
# Heat map could be improved by showing percentage for each month

p <- ggplot(df, aes(x=year, y=month, fill=avgDayPercent)) 
p + geom_tile() 
# Contrasting colour gradient and adjusting scales to highlight changes
range = dplyr::summarize(df,max = max(avgDayPercent),min = min(avgDayPercent))
p <- ggplot(df, aes(year, month, fill = avgDayPercent)) 
p + geom_tile() + 
  scale_fill_gradient2(low=muted("red"), 
                       mid="white", 
                       high=muted("blue"),
                       midpoint=8,
                       limits=c(range$min,range$max))

# Making it Interactive, 
p <- plot_ly(df, x = ~year, y = ~month, z = ~avgDayPercent,
  type = "heatmap",
  colors = colorRamp(c("#a55a54", "white","#3b3396")), # Using same default colours as R 
  hoverinfo = 'text+x+y',
  text = ~paste('Percent:', avgDayPercent,'%')
  )
rangeslider(p) #Adding more interaction to allow user to focus on dates
# Can see a whole shift in not just the most popular month but the time of year
# by following the blue path 


# Using gganimate extension library to create a better annimation
birthCountByMonth = df %>%
  dplyr::group_by(month) %>%
  dplyr::filter(year>=2000,year<=2017)

p <-ggplot(birthCountByMonth, aes(x=month, y=avgDayCount, fill=month)) +
  geom_bar(stat="identity",position = "dodge") +
  my_theme +
  ggtitle("Monthly Birth Count","{frame_time}") + 
  labs(fill = "Months",x = '', y = 'Average Daily Birth Count (births / days in month)') +
  transition_time(year) +
  ease_aes('cubic-in-out')
#animate(p, nframes = 200, fps = 10, end_pause = 20) 
#anim_save("monthly_birth_count_test.gif")


##############################################################################################################
# RQ1 MAIN VISUALIZATION
##############################################################################################################

message("RQ1: Which is the most popular month to give birth? Has this changed over the years?")

# Heat map
m <- plot_ly(df, x=~year, y=~month, z=~avgDayPercent,
             type = "heatmap",
             colorbar = list(title = "Percentage of \n Births per year"),
             colors = colorRamp(c("#a55a54", "white","#3b3396")),
             hoverinfo = 'text+x+y',
             text = ~paste('Percent:', avgDayPercent,'%'),
             width = 1100
            ) %>%
            config(displayModeBar=F) %>% 
            layout(title = "Monthly Births: England & Wales",
                   titlefont=list(family = "sans serif",size = 18),
                   yaxis=list(title="Month"),
                   xaxis=list(title="*monthly birth counts have been divided by days in each month",titlefont=list(
                     family = "Courier New, monospace",size = 10,color = "grey"))
                   ) 
p <- rangeslider(m)
p
htmlwidgets::saveWidget(as_widget(p), "birth_count_heat_map.html")

##############################################################################################################
# EXPANDING ON RQ1
##############################################################################################################

message("RQ1.1: How has monthly birth counts changed over the past decade?")

# Bar Graph, Self ranking
bardf = df %>%
  dplyr::group_by(year) %>%
  dplyr::mutate(rank = rank(avgDayCount)) %>%
  ungroup() %>% 
  arrange(desc(year), rank) 
bardf = bardf %>%
  dplyr::group_by(month) %>%
  dplyr::filter(year>=1997,year<=2017) %>%
  ungroup() 
p <- bardf %>%
  ggplot(aes(rank, group=month, fill=month)) +
  geom_tile(aes(y = avgDayCount/2, height = avgDayCount, width = 0.9)) +
  geom_text(aes(y = avgDayCount, label=month,color=month), hjust = -0.1) + # Bar labels
  coord_flip(xlim=c(0.5,12.5), ylim=c(0, 2500)) +
  theme(axis.ticks.y=element_blank(), axis.text.y=element_blank(),
        plot.title = element_text(family = "Helvetica", face = "bold", size = (18)), 
        plot.subtitle =element_text(size=18, hjust=1, face="italic", color="black"),
        legend.title = element_text(colour = "steelblue4",  face = "bold.italic", family = "Helvetica"), 
        legend.text = element_text(face = "italic", colour="steelblue4",family = "Helvetica"), 
        axis.title = element_text(family = "Helvetica", size = (10), colour = "steelblue4"),
        axis.text = element_text(colour = "steelblue4", size = (10)),
        legend.position="none") + 
  ggtitle("Monthly Birth Count: England & Wales","{closest_state}") +
  labs(x = '', y = 'Average Daily Birth Count (births / days in month)') +
  guides(fill=FALSE) +
  transition_states(year) +
  ease_aes('cubic-in-out')
animate(p, nframes = 10, fps = 1, width = 400, height = 500) # Quick render to show concept
anim_save("short_render.gif")
# Uncomment for better render (will take a lot longer)
#animate(p, nframes = 420, fps = 12, width = 400, height = 500)
#anim_save("monthly_birth_count_1997_2017.gif")


message("RQ1.2: How do birth counts vary by month?")

# Box Plot
p <-ggplot(df, aes(x=month, y=avgDayCount,fill=month)) +
  geom_boxplot() +
  geom_jitter(position = position_jitter(width = 0.2, height = 0), alpha = 1/4)+
  ggtitle("Monthly Birth Count: England & Wales 1944-2017") +
  labs(x = '', y = 'Average Daily Birth Count (births / days in month)') +
  guides(fill=FALSE) +
  #theme(axis.text.y = element_blank()) +
  my_theme 
p

png(file="box_plot_births_1944-2017.png") 
p
dev.off()
getOption("device")

message("RQ1.3: How do monthly birth counts compare by generation?")

boxDF <- df[!is.na(df$generation), ] # Removing unwanted years not categorised by generation

p <-ggplot(boxDF, aes(x=month, y=avgDayCount,fill=month)) +
  geom_boxplot() +
  geom_jitter(position = position_jitter(width = 0.2, height = 0), alpha = 1/4)+
  facet_grid(generation ~ .) +
  guides(fill=FALSE) +
  ggtitle("Monthly Birth Count by Generation","England & Wales 1944-2017") +
  labs(x = '', y = 'Average Daily Birth Count (births / days in month)') +
  my_theme +
  theme(plot.subtitle = element_text(size=12,hjust = 0)) 
p
png(file="box_plot_birth_generation.png") 
p
dev.off()
getOption("device")



message("RQ1.4: What are the trends between March and September?")

# March vs September
g <- df %>% 
  group_by(month) %>%
  plot_ly(x = ~year, y = ~avgDayPercent) %>%
  config(displayModeBar=F) %>% 
  add_lines(name = "Months", hoverinfo = "none", type = "scatter", mode = "lines", line = list(color = "lightgrey")) %>%
  add_lines(name = "March", data = filter(df, month == "March")) %>%
  add_lines(name = "September", data = filter(df, month == "September")) %>% 
  layout(title = "Monthly Birth Counts: England & Wales",
         titlefont=list(family = "sans serif",size = 18),
         yaxis=list(title="Percentage of Births per Year")
  ) 

p = rangeslider(g)
p
htmlwidgets::saveWidget(as_widget(p), "Birth_Count_MarSep_graph.html")



message("RQ1.5: Are there any patterns by season?")

Spring = df %>%
  dplyr::group_by(year) %>%
  dplyr::filter(month %in% c("December", "January", "February")) %>%
  mutate(season = "Spring", avgSeasonCount = sum(avgDayCount)) %>%
  ungroup()
Summer = df %>%
  dplyr::group_by(year) %>%
  dplyr::filter(month %in% c("March", "April", "May")) %>%
  mutate(season = "Summer",avgSeasonCount = sum(avgDayCount)) %>%
  ungroup()
Autumn = df %>%
  dplyr::group_by(year) %>%
  dplyr::filter(month %in% c("June", "July", "August")) %>%
  mutate(season = "Autumn",avgSeasonCount = sum(avgDayCount)) %>%
  ungroup()
Winter = df %>%
  dplyr::group_by(year) %>%
  dplyr::filter(month %in% c("September", "October", "November")) %>%
  mutate(season = "Winter",avgSeasonCount = sum(avgDayCount)) %>%
  ungroup()

g <- df %>% 
  plot_ly(x = ~year, y = ~avgSeasonCount) %>%
  config(displayModeBar=F) %>% 
  #add_lines(name = "Months", hoverinfo = "none", type = "scatter", mode = "lines", line = list(color = "lightgrey")) %>%
  add_lines(name = "Spring", data = Spring) %>%
  add_lines(name = "Summer", data = Summer) %>%
  add_lines(name = "Autumn", data = Autumn) %>%
  add_lines(name = "Winter", data = Winter) %>% 
  layout(title = "Birth Counts by Season: England & Wales",
         titlefont=list(family = "sans serif",size = 18),
         yaxis=list(title="Birth Count")
  ) 
p <- rangeslider(g)
p
htmlwidgets::saveWidget(as_widget(p), "birth_count_seasons_graph.html")











# Extra, non research question
# Fifty Shades Baby Boom
temp = df %>%
  filter(year >2006)
png(file="fifty_shades_boom.png", width=1000) 
p <- ggplot(temp, aes(year, month, fill = avgDayCount)) 
p + geom_tile() + 
  scale_fill_gradient2(low=muted("red"), 
                       mid="white", 
                       high=muted("blue"),
                       midpoint=1900,
                       limits=c(1700,2100)) +
  scale_x_continuous(breaks = temp$year) +
  geom_text(aes(label = round(avgDayCount, 1))) + 
  ggtitle("Monthly Births: England & Wales","Highlighted Fifty Shades of Grey Book Release Dates + Average Pregnancy Length") + 
  labs(fill = "Average Daily \nBirth Count",x = '*Average Daily Birth Count (montly birth count / days in month)', y = 'Month') +
 # annotate("segment", x = c(2013,2013,2013), xend = c(2012.5,2012.5,2012.5), y = c("January","May","September"), yend = c("January","May","September"), colour = "green", size=3, alpha=0.5, arrow=arrow()) +
  theme(plot.title = element_text(family = "Helvetica",face = "bold", size = 18,hjust = 0.5),plot.subtitle=element_text(size=12,hjust = 0.5),
        plot.background = element_rect(fill = "transparent", color = NA),
        panel.background = element_rect(fill = "transparent"),panel.grid = element_blank(),
        axis.title.x = element_text(family = "Helvetica",color="darkgrey", size =12)) +
  geom_rect(aes(xmin = 2011.5 , xmax =2012.5, ymin = 0.5, ymax = 1.5), fill = "transparent", color = "orange", size = 0.9) +
  geom_rect(aes(xmin = 2011.5 , xmax =2012.5, ymin = 4.5, ymax = 5.5), fill = "transparent", color = "orange", size = 0.9) +
  geom_rect(aes(xmin = 2011.5 , xmax =2012.5, ymin = 8.5, ymax = 9.5), fill = "transparent", color = "orange", size = 0.9) 

dev.off()
