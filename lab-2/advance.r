##############################################################################################################
# G53FIV LAB TWO
#
# Using R and the dplyr library for data mainpulation
##############################################################################################################
library(ggplot2)
library(dplyr)  
library(data.table) 
library(plyr) 
library(gridExtra) 

R_DATA_GENERAL <<- "/Users/benmorse/Documents/R/lab-2/"
SEP <- "\t"
SEP2 <- ","

#url if needed https://raw.githubusercontent.com/genomicsclass/dagdata/master/inst/extdata/msleep_ggplot2.csv

# Load dataset msleep_ggplot2.csv
msleepData <- read.table(paste(R_DATA_GENERAL, "msleep_ggplot2.csv", sep=""), quote="\"", header=T, comment.char="", sep=SEP2)
# Display head of dataset
head(msleepData)

#(1) use both the R base functions and dplyr functions to select a subset of the columns: 
#   (a) a data table contains only the name and the sleep_total column; 
#   (b) the data table that only excludes the name column.

nameAndSleepTotal = select(msleepData, name, sleep_total)
head(nameAndSleepTotal)
message("....")
tail(nameAndSleepTotal)

sleepNoNameColumn = select(msleepData, -name)
head(sleepNoNameColumn)

#(2) use dplyr to generate the data table that contain all columns that start with the character string “sleep”.
columnsStartWithSleep = select(msleepData, starts_with("sleep"))
head(columnsStartWithSleep)

#(3) use both R base functions and dplyr functions to select a subset of the data points: 
#    (a) a data table that contains the rows for mammals that sleep a total of more than 16 hours; 
#    (b) a data table only contains the rows for mammals that sleep a total of more than 16 hours and have a body weight of greater than 1 kilogram; 
#    (c) a data table that contains the rows for mammals in the “Perissodactyla” and “Primates” taxonomic rank.

sleepMoreThan = filter(msleepData, sleep_total >= 16)
head(sleepMoreThan)

sleepMoreThanAndWeigh = filter(msleepData, sleep_total >= 16, bodywt >= 1)
head(sleepMoreThanAndWeigh)

taxonomicRank = filter(msleepData, order %in% c("Perissodactyla", "Primates"))
head(taxonomicRank)

#(4) use dplyr functions to reorder the data according to first taxonomic rank and secondly by sleep_total (decreasing order).

sleepReorder = arrange(select(msleepData,name, order, sleep_total),order, desc(sleep_total))
head(sleepReorder)

# Can be improved with piping
# the operator: %>% 
# Allows you to pipe the output from one function to the input of another function. 
# Instead of nesting functions (reading from the inside to the outside), the idea of of piping is to read the functions from left to right.

# Example:
head(select(msleepData, name, sleep_total))
# Can be written as:
msleepData %>% 
  select(name, sleep_total) %>% 
  head

# Answer to 4 using piping 
msleepData %>% 
  select(name, order, sleep_total) %>%
  arrange(order, desc(sleep_total)) %>% 
  head

#(5) try to use the pipe operator to create a data table that 
#    contains only the columns name, order and sleep_total while 
#    arranging the rows in the sleep_total column in a descending 
#    order and only show those mammals that sleep more than 16 hours.

x = msleepData %>% 
  select(name, order, sleep_total) %>%
  arrange(order, desc(sleep_total)) %>%
  filter(sleep_total >= 16)

head(x)

#(6) use both R base functions and dplyr to create a data table that 
#    contains a new column called rem_proportion which is the ratio 
#    of rem sleep to total amount of sleep.

newTable = mutate(msleepData, rem_proportion = sleep_rem / sleep_total)
head(newTable)

#(7) use dplyr functions to: 
#    (a) compute the average number of hours of sleep for all mammals; 
#    (b) compute the min, average, max number of hours of sleep for 
#        mammals according to their taxonomic rank (order).

msleepData %>% 
  dplyr::summarise(avg_sleep = mean(sleep_total), 
                   min_sleep = min(sleep_total), 
                   max_sleep = max(sleep_total)) %>%
  head

# Methods conflict between plyr and dplyr
conflicts()
# Solutions:
#         dplyr::summarise
#         load plyr first
#         unload plyr detach("package:plyr", unload=TRUE) 

dataTable = msleepData %>% 
  group_by(order) %>%
  dplyr::summarise(avg_sleep = mean(sleep_total), 
            min_sleep = min(sleep_total), 
            max_sleep = max(sleep_total))

#(8) Save data tables as CSV files

write.csv(dataTable,file="/Users/benmorse/Documents/R/lab-2/testWrite.csv")

# Try to examine the above data in an exploratory 
# fashion and try to use pipe operator to do the 
# data manipulations and then plot graphs in one go, 
# which provide some insights on the data.


# Data table that contains the rows for mammals that sleep a total of more than 16 hours; 
sleepMoreThan = filter(msleepData, sleep_total >= 16)
head(sleepMoreThan)
# Data table only contains the rows for mammals that sleep a total of more than 16 hours and have a body weight of greater than 1 kilogram; 
sleepMoreThanAndWeigh = filter(msleepData, sleep_total >= 16, bodywt >= 1)
head(sleepMoreThanAndWeigh)
# Data table that contains the rows for mammals in the “Perissodactyla” and “Primates” taxonomic rank.
taxonomicRank = filter(msleepData, order %in% c("Perissodactyla", "Primates"))
head(taxonomicRank)

# Histogram
hist(msleepData$sleep_total, freq=NULL, density=NULL, breaks=12,
     xlab="Sleep Total", ylab="Frequency", main="Histogram of Sleep Total")


histogram <- ggplot(data=msleepData, aes(x=sleep_total))
histogram + geom_histogram(binwidth=0.2, color="black", aes(fill=order)) + 
  xlab("Sleep Total") +  ylab("Frequency") + ggtitle("Histogram of Taxonomic Sleep totals")


