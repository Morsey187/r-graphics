# R
Projects and exercises using the R statistical programming language

## Directories
/birth-statistics
> University Project

/basic-tutorial
> Following: https://www.youtube.com/watch?v=s3FozVfd7q4

/lab-1, /lab-2 and /property-prices
> University lab sessions


## Useful links for R
	https://www.r-bloggers.com/
	https://www.rstudio.com/resources/cheatsheets/
	https://bookdown.org/yihui/rmarkdown/

## Good tutorials for basic plotting
	https://www.analyticsvidhya.com/blog/2015/07/guide-data-visualization-r/
	https://www.hackerearth.com/blog/machine-learning/data-visualization-techniques/
	https://www.hackerearth.com/blog/machine-learning/data-visualization-for-beginners-part-2/
	https://www.hackerearth.com/blog/machine-learning/data-visualization-for-beginners-part-3/

# ggplot2 
The Grammar of Graphics
## ggplot2 Useful links

	ggplot2 extensions http://www.ggplot2-exts.org/gallery/
	ggplot2 Quickref http://r-statistics.co/ggplot2-cheatsheet.html
	R Graphics Cookbook(Graphs) http://www.cookbook-r.com/Graphs/
	A bit of everything https://www.ling.upenn.edu/~joseff/avml2012/
	ggmap cheetsheet http://stat405.had.co.nz/ggmap.pdf

# dplyr
## dplyr Useful links
https://genomicsclass.github.io/book/pages/dplyr_tutorial.html

# Rmd
https://www.youtube.com/watch?v=tKUufzpoHDE
https://bookdown.org/yihui/rmarkdown/notebook.html
